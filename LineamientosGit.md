# Guía de Lineamientos para el Uso de Git

## Estructura de Ramas de Trabajo:

Creación de una rama por cada feature (módulo o funcionalidad nueva), fix (corrección), hot fix (corrección de emergencia), refactor (refactorización de código o optimización).

- `git checkout -b {nombre_rama_nueva}`: Crear nueva rama y moverse a la rama nueva.

### Prefijos de Ramas:

- FX: fix, es una corrección de un módulo o errores que no son urgentes.
- HF: hot fix, es una corrección urgente.
- FE: feature, es un módulo nuevo o una funcionalidad adicional.

- Estructura Final: [Prefijo][Día][Mes][Año]-[Nombre_relacionado]

### Ejemplos de Ramas:

- FX190423-curriculos → Fix del día 19 de Abril del 2023 en curriculos
- FE010123-dashboard → Feature 01 de Enero del 2023 en dashboard
- HF311223-programas → Hot Fix 31 de Diciembre del 2023 en programas
- FX270523-retos-progressBar -> Fix del día 27 de mayo del 2023 en retos en progressBar

---

## Estructura de Commits:

Estructura a seguir con cada commit que se genere.

### Prefijos de Commits:

- [Add] commit que agrega código.
- [Update] commit que actualiza código.
- [Delete] commit para cuando se borra código.

### Prefijos de Merge request:

- [Feature] commit que finaliza un módulo o una nueva funcionalidad.
- [Fix] commit para hacer correcciones.
- [Hot fix] commit para hacer un arreglo de emergencia.
- [Refactor] commit para optimizar código.

### Ejemplos de Commits:

- `git commit -m "[Feature/Add/Update/Fix/Hot-Fix/Delete/Refactor] Descripción General" -m "Descripción detallada"`
- `git commit -m "[Feature] Módulo curriculos" -m "Modulos de lecciones curriculos quedaron organizados y el CSS también"`

