# Comandos:

1. En caso de que hayan archivos nuevos:
`git add .`

2. Ver las diferencias en el código:
`git diff HEAD`

# Prompt:

¿Podrías darme un resumen en español, detallado y descriptivo basándote en los siguientes cambios?

> Redáctalo como si fuera un mensaje de commit. Dame primero el nombre del commit y luego el resumen.
> Dame una etiqueta ('[Feature]', '[Add]', '[Update]', '[Fix]', '[Hot-Fix]', '[Delete]', '[Refactor]', etc) antes del nombre del commit.
> Dame sólo 1 nombre de commit que resuma todos los cambios, que sea  lo más conciso posible.

```bash
Enviar el código del comando git diff HEAD
```
